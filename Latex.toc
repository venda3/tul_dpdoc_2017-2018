\babel@toc {czech}{}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{12}{chapter.1}
\contentsline {section}{\numberline {1.1}C\IeC {\'\i }le pr\IeC {\'a}ce}{13}{section.1.1}
\contentsline {chapter}{\numberline {2}Struktura navr\IeC {\v z}en\IeC {\'e} multiplatformn\IeC {\'\i } aplikace}{14}{chapter.2}
\contentsline {chapter}{\numberline {3}Vyvinut\IeC {\'e} moduly}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}Parametrizace}{16}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}P\IeC {\v r}\IeC {\'\i }prava sign\IeC {\'a}lu}{17}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}MelFBank p\IeC {\v r}\IeC {\'\i }znaky}{21}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Detekce \IeC {\v r}e\IeC {\v c}ov\IeC {\'y}ch segment\IeC {\r u}}{26}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Pou\IeC {\v z}it\IeC {\'a} neuronov\IeC {\'a} s\IeC {\'\i }\IeC {\v t}}{26}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Vyhlazen\IeC {\'\i } v\IeC {\'y}stupu neuronov\IeC {\'e} s\IeC {\'\i }t\IeC {\v e}}{27}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Odes\IeC {\'\i }latel a rozpozn\IeC {\'a}n\IeC {\'\i } hlasov\IeC {\'e}ho povelu}{32}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Nekorektn\IeC {\'\i } rozpozn\IeC {\'a}n\IeC {\'\i }}{33}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Vykonavatel hlasov\IeC {\'e}ho povelu}{36}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Knihovna RobotJS}{36}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Struktura povel\IeC {\r u}}{37}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Popis vytvo\IeC {\v r}en\IeC {\'e}ho u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{38}{chapter.4}
\contentsline {section}{\numberline {4.1}Angular}{38}{section.4.1}
\contentsline {section}{\numberline {4.2}Electron}{39}{section.4.2}
\contentsline {section}{\numberline {4.3}Vytvo\IeC {\v r}en\IeC {\'\i } aplikace pomoc\IeC {\'\i } knihovny Electron}{39}{section.4.3}
\contentsline {section}{\numberline {4.4}Distribuce aplikace}{45}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Zabalen\IeC {\'\i } aplikace}{45}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Vytvo\IeC {\v r}en\IeC {\'\i } instal\IeC {\'a}toru}{46}{subsection.4.4.2}
\contentsline {chapter}{\numberline {5}Experiment\IeC {\'a}ln\IeC {\'\i } vyhodnocen\IeC {\'\i }}{48}{chapter.5}
\contentsline {section}{\numberline {5.1}Vyhodnocen\IeC {\'\i } detektoru}{48}{section.5.1}
\contentsline {section}{\numberline {5.2}Vyhodnocen\IeC {\'\i } rozpozn\IeC {\'a}va\IeC {\v c}e}{49}{section.5.2}
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}v\IeC {\v e}r}{51}{chapter.6}
\contentsline {section}{\numberline {6.1}P\IeC {\v r}\IeC {\'\i }nosy pr\IeC {\'a}ce}{52}{section.6.1}
\contentsline {section}{\numberline {6.2}Dal\IeC {\v s}\IeC {\'\i } rozvoj aplikace}{53}{section.6.2}
\contentsline {chapter}{Literatura}{54}{chapter*.28}
\contentsline {chapter}{P\IeC {\v r}\IeC {\'\i }lohy}{57}{chapter*.28}
\contentsline {section}{P\IeC {\v r}\IeC {\'\i }loha A: Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{57}{appendix*.29}
