hz = 1:4000;
mel = 1127 * log(1 + hz./700);
plot(hz, mel);
xlabel('Hertzova stupnice');
ylabel('Melova stupnice');

h = hamming(400);
plot(h);
xlabel('Vzorky [-]');
ylabel('Amplituda [-]');

plot(melfilter(10, 1:8000)');
xlabel('Frekvence [hz]');
ylabel('Amplituda [-]');

x = -2:0.1:2;
plot(x, tanh(x))

y = x;
y(x < -1) = -1;
y(x > 1) = 1;
plot(x,y)

plot(x,exp(x - m)./sum(exp(x - m)))

plot(old)
hold on
plot(new)
xlabel('�as [s]')
ylabel('Pam� [kB]')
legend({'P�vodn�', 'Nov�'}, 'Location', 'northwest')

t=(0:(length(data)-1))/fs;
plot(t,data)
hold on;line([0.67 0.67], [min(data) max(data)], 'Color', 'red')
xlabel('�as [s]')
ylabel('Amplituda [-]')
hold off